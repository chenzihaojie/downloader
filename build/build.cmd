cd /d "%~dp0"
::Archive creation
if NOT EXIST "bin\AutoHotkeyU32.exe" exit /b 1
rmdir /q /s temp 2>NUL
del temp.7z 2>NUL
del ..\uupdownloader*.zip 2>NUL
del ..\uupdownloader*.7z 2>NUL

mkdir temp
mkdir temp\files
xcopy /cherkyq ..\files temp\files\files\
xcopy /cherkyq ..\include temp\files\include\
copy ..\uupdownloader.ahk temp\files\
copy bin\AutoHotkeyU32.exe temp\files\AutoHotkey.exe
copy src\run.ahk temp\files\run.ahk
copy src\run.cmd temp\uupdownloader.cmd
bin\AutoHotkeyU32.exe /ErrorStdOut src\touchdir.ahk temp
if %errorlevel% NEQ 0 exit /b %errorlevel%

..\files\7za.exe -mx9 a ..\uupdownloader.7z .\temp\*
..\files\7za.exe -mx9 a ..\uupdownloader.zip .\temp\*

bin\AutoHotkeyU32.exe /ErrorStdOut src\genversiontable.ahk temp\filenames.cmd
if %errorlevel% NEQ 0 exit /b %errorlevel%

::Final naming of files
call temp\filenames.cmd
ren ..\uupdownloader.7z "%fileNames%.7z"
ren ..\uupdownloader.zip "%fileNames%.zip"

::Cleanup
rmdir /q /s temp

::Error if one of files does not exist
if NOT EXIST "..\%fileNames%.7z" exit /b 1
if NOT EXIST "..\%fileNames%.zip" exit /b 1
