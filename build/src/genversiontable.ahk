﻿WorkDir := A_WorkingDir
#Include %A_ScriptDir%\..\..\include\header.ahk
#Include %A_ScriptDir%\..\..\include\appinfo.ahk
SetWorkingDir, %WorkDir%

File1 = %1%
If File1 =
{
    ExitApp, 1
}

FileAppend, set fileNames=%AppFileName%, %File1%
