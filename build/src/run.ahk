﻿#Include %A_ScriptDir%\include\header.ahk
#Include %A_ScriptDir%\include\appinfo.ahk
#Include %A_ScriptDir%\include\language.ahk

Arg1 := A_Args[1]
If(Arg1 == 1) {
    Show = Max
} else If(Arg1 == 2) {
    Show = Min
} else If(Arg1 == 3) {
    Show = Hide
} else {
    Show =
}

CmdLine =
for n, param in A_Args
{
    If(n == 1) {
        continue
    }

    If(n < 3) {
       CmdLine := param
    } else {
        CmdLine := CmdLine " " param
    }
}

If(CmdLine == "") {
    MsgBox, 0, %AppName%,
(
Usage: %A_ScriptName% show quote_escaped_command_line

show:
0 - normal
1 - max
2 - min
3 - hide

Quotes are escaped using \
Example: \"cmd.exe\"
)
    ExitApp 0x80070057
}

Run, *RunAs %CmdLine%,, %Show% UseErrorLevel
if(ErrorLevel == "ERROR") {
    Error := A_LastError
    Gui, +OwnDialogs
    If(Error == 1223) {
        MsgBox, 0x10, %AppName%, %text_NoAdmin%
    } else {
        MsgBox, 0x10, %AppName%, Failed to start: %CmdLine%
    }
    ExitApp %Error%
}

ExitApp 0
